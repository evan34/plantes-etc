import React, { useState, useEffect } from 'react'
import axios from 'axios';
import { Button, Card, CardBody, CardFooter, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';

function CreateAromatic(props) {
    const [aromatic, setAromatic] = useState({ Name: '', Designation: '', Location: '' });
    const [showLoading, setShowLoading] = useState(false);
    const apiUrl = "http://localhost:1337";

    const InsertAromatic = (e) => {
        e.preventDefault();
        //debugger;
        const data = { Name: aromatic.Name, Designation: aromatic.Designation, Location: aromatic.Location };
        axios.post(apiUrl, data)
            .then((result) => {
                props.history.push('/Aromatics')
            });
    };
    const onChange = (e) => {
        e.persist();
        //debugger;
        setAromatic({ ...aromatic, [e.target.name]: e.target.value });
    }

    return (
        <div className="app flex-row align-items-center">
            <Container>
                <Row className="justify-content-center">
                    <Col md="12" lg="10" xl="8">
                        <Card className="mx-4">
                            <CardBody className="p-4">
                                <Form onSubmit={InsertAromatic}>
                                    <h1>Register</h1>
                                    <InputGroup className="mb-3">

                                        <Input type="text" name="Name" id="Name" placeholder="Name" value={aromatic.Name} onChange={onChange} />
                                    </InputGroup>
                                    <InputGroup className="mb-3">

                                        <Input type="text" placeholder="Designation" name="Designation" id="Designation" value={aromatic.Designation} onChange={onChange} />
                                    </InputGroup>
                                    <InputGroup className="mb-3">

                                        <Input type="text" placeholder="Location" name="Location" id="Location" value={aromatic.Location} onChange={onChange} />
                                    </InputGroup>

                                    <CardFooter className="p-4">
                                        <Row>
                                            <Col xs="12" sm="6">
                                                <Button type="submit" className="btn btn-info mb-1" block><span>Save</span></Button>
                                            </Col>
                                            <Col xs="12" sm="6">
                                                <Button className="btn btn-info mb-1" block><span>Cancel</span></Button>
                                            </Col>
                                        </Row>
                                    </CardFooter>
                                </Form>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}
export default CreateAromatic; 