import React from 'react'
import { Badge, Card, CardBody, CardHeader, Col, Pagination, PaginationItem, PaginationLink, Row, Table } from 'reactstrap';
import axios from 'axios';
import { useState, useEffect } from 'react'

function Aromatics(props) {
    const [data, setData] = useState([]);

    useEffect(() => {
        const GetData = async () => {
            const result = await axios('http://localhost:1337/aromatics');
            console.log(result);
            setData(result.data);
        };

        GetData();
    }, []);
    const deleteeployee = (id) => {
        //debugger;
        axios.delete('http://localhost:1337/aromatics?id=' + id)
            .then((result) => {
                props.history.push('/Aromatics')
            });
    };
    const editemployee = (id) => {
        props.history.push({
            pathname: '/edit/' + id
        });
    };

    return (
        <div className="animated fadeIn">
            <Row>
                <Col>
                    <Card>
                        <CardHeader>
                            <i className="fa fa-align-justify"></i> Employee List
              </CardHeader>
                        <CardBody>
                            <Table hover bordered striped responsive size="sm">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Department</th>
                                        <th>Age</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        data.map((item) => {
                                            return <tr key={item.id}>
                                                <td>{item.name}</td>
                                                <td>{item.designation}</td>
                                                <td>{item.location}</td>
                                                <td>
                                                    <div className="btn-group">
                                                        <button className="btn btn-warning" onClick={() => { editemployee(item.Id) }}>Edit</button>
                                                        <button className="btn btn-warning" onClick={() => { deleteeployee(item.Id) }}>Delete</button>
                                                    </div>
                                                </td>
                                            </tr>
                                        })}
                                </tbody>
                            </Table>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        </div>
    )
}

export default Aromatics  