import React from 'react';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom'

import Aromatics from './components/Aromatics/Aromatics'
import AddAromatic from './components/Aromatics/AddAromatic'
import EditAromatic from './components/Aromatics/EditAromatic'
import './App.css';

function App() {
  return (
    <div className="App">
      <Router>
        <div className="container">
          <nav className="btn btn-warning navbar navbar-expand-lg navheader">
            <div className="collapse navbar-collapse" >

              <ul className="navbar-nav mr-auto">
                <li className="nav-item">
                  <Link to={'/AddAromatic'} className="nav-link">Ajouter une aromatique</Link>
                </li>
                <li className="nav-item">
                  <Link to={'/Aromatics'} className="nav-link">Employee List</Link>
                </li>
              </ul>
            </div>
          </nav> <br />
          <Switch>
            <Route exact path='/AddAromatic' component={AddAromatic} />
            <Route path='/edit/:id' component={EditAromatic} />
            <Route path='/aromatics' component={Aromatics} />
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
